#!/usr/bin/env python

from json import load as json_load
from datetime import timedelta, datetime
from requests import get as requests_get
from argparse import ArgumentParser
from sys import stderr, stdout

DEFAULT_DURATION = 45
DEFAULT_STATE = 'BW'

def make_subject(start: datetime, end: datetime, subject: str, **kwargs):
    color = None
    subject_meta = kwargs.get('subjects', {}).get(subject, {})
    if 'long' in subject_meta:
        subject = subject_meta['long']
    if 'color' in subject_meta:
        color = subject_meta['color']
        if color in kwargs.get('colors', {}):
            color = kwargs['colors'][color]

    if 'lesson' in kwargs:
        subject = '{}: {}'.format(kwargs['lesson'], subject)

    return {
        'start': start.time(),
        'end': end.time(),
        'subject': subject,
        'color': None if subject is None else color,
        'school': None if subject is None else kwargs.get('school'),
        'grade': None if subject is None else kwargs.get('grade')
    }

def get_empty_day(data):
    duration = data.get('meta', {}).get('lessonsduration', DEFAULT_DURATION)

    result = []
    for lesson in data.get('meta', {}).get('lessons', []):
        start = datetime.strptime(lesson['start'], '%H:%M') 
        end = start + timedelta(minutes=duration)
        result.append(make_subject(start, end, None))
    return result

def generate_day(index: int, day: str, data: any, cancel_missing: bool):
    duration = data.get('meta', {}).get('lessonsduration', DEFAULT_DURATION)
    subjects = []

    meta_starts = []
    for start in data.get('meta', {}).get('lessons', []):
        meta_starts.append({
            'index': start['lesson'],
            'start': datetime.strptime(start['start'], '%H:%M') 
        })

    starts_seen = []
    lesson = 1

    for entry in data.get(day, []):
        if 'subject' not in entry:
            continue
 
        if 'start' in entry:
            start = datetime.strptime(entry['start'], '%H:%M')
        elif 'lesson' in entry:
            lesson = entry['lesson']
            start = list(filter(lambda x: x['index'] == lesson, meta_starts))[0]['start']
        else:
            continue

        starts_seen.append(start)

        end = start + timedelta(minutes=duration)

        subjects.append(make_subject(start, end, entry['subject'], lesson=lesson, **data.get('meta')))

        lesson += 1
    
    if cancel_missing:
        for missing_start in filter(lambda x: x['start'] not in starts_seen, meta_starts):
            start = missing_start['start']
            end = start + timedelta(minutes=duration)
            subjects.append(make_subject(start, end, None))

    return {
        'day': index,
        'subjects': subjects
    }

def get_subjects(day, days):
    results = list(filter(lambda x: x['day'] == day, days))
    return results[0].get('subjects', []) if results else []

def load_vacations(data):
    state_code = data.get('meta', {}).get('state', DEFAULT_STATE)
    resp = requests_get(f'https://ferien-api.de/api/v1/holidays/{state_code}')
    vacations = [{
        'start': datetime.strptime(vacation['start'], '%Y-%m-%dT%H:%MZ').date(),
        'end': datetime.strptime(vacation['end'], '%Y-%m-%dT%H:%MZ').date(),
        'name': vacation['name']
    } for vacation in resp.json()] + [{
        'start': datetime.strptime(vacation['start'], '%Y-%m-%d').date(),
        'end': datetime.strptime(vacation['end'], '%Y-%m-%d').date(),
        'name': vacation['name'].lower()
    } for vacation in data.get('meta', {}).get('vacations', [])]

    return vacations

def load_holidays(first_day, last_day, data):
    state_code = data.get('meta', {}).get('state', DEFAULT_STATE)
    holidays = []
    for year in [first_day.year, last_day.year]:
        resp = requests_get(f'https://feiertage-api.de/api/?jahr={year}&nur_land={state_code}')
        for holiday, data in resp.json().items():
            date = datetime.strptime(data['datum'], '%Y-%m-%d').date()
            if date >= first_day and date <= last_day:
                holidays.append({
                    'date': date,
                    'name': holiday
                })
    return holidays

def generate_header(file):
    print('''BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:Stundenplan
PRODID:-//stundenplan.gmeiner.eu//iCal
X-WR-CALDESC:Stundenplan
X-WR-TIMEZONE:Europe/Berlin
CALSCALE:GREGORIAN
''', file=file)

def get_uid(day: datetime, subject: any):
    return 'STUNDENPLAN-' + datetime.combine(day, subject['start']).strftime('%Y%m%dT%H%M%S')

def get_summary(subject: any):
    if 'subject' not in subject or subject['subject'] is None:
        return ''
    else:
        return subject['subject']

def get_location(subject: any):
    result = []
    if 'school' in subject and subject['school']:
        result.append(subject['school'])
    if 'grade' in subject and subject['grade']:
        result.append(subject['grade'])
    return ', '.join(result)

def generate_event(file, day: datetime, subject: any):
    print(f'''BEGIN:VEVENT
UID:{get_uid(day, subject)}
SUMMARY:{get_summary(subject)}
DTSTART:{datetime.combine(day, subject['start']).strftime('%Y%m%dT%H%M%S')}
DTEND:{datetime.combine(day, subject['end']).strftime('%Y%m%dT%H%M%S')}
DTSTAMP:{datetime.now().strftime('%Y%m%dT%H%M%SZ')}
LAST-MODIFIED:{datetime.now().strftime('%Y%m%dT%H%M%SZ')}
LOCATION:{get_location(subject)}
TRANSP:TRANSPARENT
SEQUENCE:0
CLASS:PUBLIC
PRIORITY:9
STATUS:{'CANCELLED' if subject['subject'] is None else 'CONFIRMED'}
COLOR:{subject['color'] if subject['color'] else ''}
URL:http://stundenplan.gmeiner.eu
END:VEVENT
''', file=file)

def generate_footer(file):
    print('''END:VCALENDAR
''', file=file)


def get_output(filename: str):
    if filename == '-':
        return stdout
    else:
        return open(filename, 'w')

def main(input_file: str, output_file: str, valid: str, clear_vacations: bool, cancel_missing: bool):
    if valid:
        if valid == 'today':
            valid_from = datetime.now().date()
        elif valid == 'tomorrow':
            valid_from = (datetime.now() + timedelta(days=1)).date()
        else:
            valid_from = datetime.strptime(valid, '%Y-%m-%d').date()
    else:
        valid_from = None

    reference_date = valid_from if valid_from else datetime.now().date()

    days = []
    with open(input_file, 'r') as fp:
        data = json_load(fp)

        first_day = None
        last_day = None

        vacations = load_vacations(data)
        for summer_vacation in filter(lambda x: x['name'] == 'sommerferien', vacations):
            if summer_vacation['start'] < reference_date:
                if first_day is None or first_day < summer_vacation['end']:
                    first_day = summer_vacation['end']
            else:
                if last_day is None or last_day > summer_vacation['start']:
                    last_day = summer_vacation['start']

        relevant_vacations = list(filter(lambda x:
                x['start'] > first_day and x['start'] < last_day, vacations))
        relevant_holidays = load_holidays(first_day, last_day, data)

        for index, day in enumerate(['monday', 'tuesday', 'wednesday', 'thursday', 'friday']):
            days.append(generate_day(index, day, data, cancel_missing))

        empty_day = get_empty_day(data)

    if valid_from:
        first_day = reference_date

    with get_output(output_file) as fp:
        try:
            count = 0
            generate_header(fp)

            day = first_day
            while day < last_day:

                in_vacations = list(filter(lambda x: x['start'] <= day and x['end'] >= day, relevant_vacations))
                on_holiday = list(filter(lambda x: x['date'] == day, relevant_holidays))
                if in_vacations or on_holiday:
                    if clear_vacations:
                        for entry in empty_day:
                            generate_event(fp, day, entry)
                            count += 1
                else:
                    for subject in get_subjects(day.weekday(), days):
                        generate_event(fp, day, subject)
                        count += 1

                day += timedelta(days=1)

            generate_footer(fp)

            print(f'Total events generated: {count}', file=stderr)
        except BrokenPipeError:
            pass

if __name__ == '__main__':
    parser = ArgumentParser(description='Stundenplan generator')
    parser.add_argument('--valid', '-v', metavar='yyyy-mm-dd', required=False,
        help='timetable becomes valid on and after this date (in format "yyyy-mm-dd", also accepts "today" or "tomorrow")')
    parser.add_argument('--clear', '-c', action='store_true', default=False,
        help='clear existing events during vacations')
    parser.add_argument('--no-cancel', '-n', action='store_true', default=False,
        help='do not generate cancellation events')
    parser.add_argument('--input', '-i', metavar='stundenplan.json', required=True,
        help='input JSON file')
    parser.add_argument('--output', '-o', metavar='stundenplan.ics', required=True,
        help='generated iCal file')
    args = parser.parse_args()
    main(args.input, args.output, args.valid, args.clear, not args.no_cancel)
