# stundenplan-generator

Generates an iCal file from the timetable data.
Considers regional holidays and school vacations.


## Usage

```
./generate.py -i example.json -o example.ics
```

By default events are generated for the complete current year. A start date may be provided after which events will be generated for the rest of the respective year. This may be useful when a timetable changes after a particular date:

```
./generate.py -i example.json -o example.ics -v 2022-04-01
```

An output parameter of "-" prints the iCal data to the console:
```
./generate.py -i example.json -o -
```


## Remarks

Data structure should be self-explanatory.


## Thanks

Big thanks to:

* https://feiertage-api.de/ for public holidays information used here
* https://ferien-api.de/ for school vacations information used here
